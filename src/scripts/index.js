const SHIELD_MOD = {
    scope: "shield-mod",
    settings: {
        useMultipleShields: "useMultipleShields",
    },
    flags: {
        baseValues: "baseValues",
        currentMods: "currentMods",
        update: "update",
    },
};

Hooks.on("init", function() {
    game.settings.register(SHIELD_MOD.scope, SHIELD_MOD.settings.useMultipleShields, {
        name: "Allow stacking AC bonuses",
        hint: "Adds the AC bonuses from multiple shileds, instead of using the highest.",
        scope: "world",
        config: true,
        type: Boolean,
        default: false,
        onChange: recalculateAll,
    });
});

Hooks.on("ready", async function () {
    for (let actor of game.actors.values()) {
        if (!actor.owner) continue;
        if (getProperty(actor, "data.type") !== "character") continue;

        await initFlags(actor);
    }
});

Hooks.on("createActor", async function (actor, opt, userId) {
    if (userId != game.userId) return;
    if (getProperty(actor, "data.type") !== "character") return;

    await initFlags(actor);
});

Hooks.on("updateOwnedItem", async function (actor, item, data, opt, userId) {
    if (userId != game.userId) return;
    if (getProperty(actor, "data.type") !== "character") return;

    // item has an armor value
    let armorValue = getProperty(item, "data.armor.value");
    if (!Number.isInteger(armorValue) || armorValue === 0) return;

    // item is shield-type armor
    if (getProperty(item, "data.armor.type") !== "shield") return;

    // event is equipping/unequipping item
    let equipped = getProperty(data, "data.equipped");
    if (equipped === undefined) return;

    if (equipped) {
        await addArmorMod(actor, item);
    } else {
        await removeArmorMod(actor, item);
    }
});

Hooks.on("updateActor", async function (actor, data, opt, userId) {
    if (userId != game.userId) return;
    if (getProperty(actor, "data.type") !== "character") return;

    // event is not triggered by the armor mod
    if (getProperty(data, `flags.${SHIELD_MOD.scope}.${SHIELD_MOD.flags.update}`) !== undefined) return;

    // event is changing AC
    let newTotalAc = getProperty(data, "data.attributes.ac.value")
    if (newTotalAc === undefined) return;
    
    let baseValues = actor.getFlag(SHIELD_MOD.scope, SHIELD_MOD.flags.baseValues);
    let currentMods = actor.getFlag(SHIELD_MOD.scope, SHIELD_MOD.flags.currentMods);

    let modAc = 0;
    for (mod of currentMods) {
        modAc += mod.ac;
    }

    // set new base AC offset by our mods
    baseValues.ac = newTotalAc - modAc;

    await actor.unsetFlag(SHIELD_MOD.scope, SHIELD_MOD.flags.baseValues);
    await actor.setFlag(SHIELD_MOD.scope, SHIELD_MOD.flags.baseValues, baseValues);
});

async function addArmorMod(actor, item) {
    let currentMods = actor.getFlag(SHIELD_MOD.scope, SHIELD_MOD.flags.currentMods);
    let i = currentMods.findIndex((mod) => mod.itemId === item._id);
    
    if (i === -1) {
        currentMods.push({
            itemId: item._id,
            ac: getProperty(item, "data.armor.value")
        });
    }
    
    await updateModAc(actor, currentMods);
}

async function removeArmorMod(actor, item) {
    let currentMods = actor.getFlag(SHIELD_MOD.scope, SHIELD_MOD.flags.currentMods);
    let i = currentMods.findIndex((mod) => mod.itemId === item._id);

    if (i > -1) {
        currentMods.splice(i, 1);
    }

    await updateModAc(actor, currentMods);
}

async function updateModAc(actor, currentMods) {
    let baseAc = actor.getFlag(SHIELD_MOD.scope, SHIELD_MOD.flags.baseValues).ac;

    let shieldBonus = 0;
    if (game.settings.get(SHIELD_MOD.scope, SHIELD_MOD.settings.useMultipleShields)) {
        for (mod of currentMods) {
            shieldBonus += mod.ac;
        }
    } else {
        for (mod of currentMods) {
            shieldBonus = mod.ac > shieldBonus ? mod.ac : shieldBonus;
        }
    }

    let totalAc = baseAc + shieldBonus;

    await actor.unsetFlag(SHIELD_MOD.scope, SHIELD_MOD.flags.currentMods);
    await actor.update({
        "data.attributes.ac.value": totalAc,
        [`flags.${SHIELD_MOD.scope}.${SHIELD_MOD.flags.currentMods}`]: currentMods,
        [`flags.${SHIELD_MOD.scope}.${SHIELD_MOD.flags.update}`]: randomID(),
    });
}

async function recalculateAll() {
    for (let actor of game.actors.values()) {
        if (!actor.owner) continue;
        if (getProperty(actor, "data.type") !== "character") continue;

        await updateModAc(actor, actor.getFlag(SHIELD_MOD.scope, SHIELD_MOD.flags.currentMods));
    }
}

async function initFlags(actor) {
    if (!actor.getFlag(SHIELD_MOD.scope, SHIELD_MOD.flags.baseValues)) {
        await actor.setFlag(SHIELD_MOD.scope, SHIELD_MOD.flags.baseValues, {
            ac: getProperty(actor, "data.data.attributes.ac.value"),
        });
    }

    if (!actor.getFlag(SHIELD_MOD.scope, SHIELD_MOD.flags.currentMods)) {
        await actor.setFlag(SHIELD_MOD.scope, SHIELD_MOD.flags.currentMods, []);
    }
}

console.log(`Shield Mod (5e) loaded!`);
